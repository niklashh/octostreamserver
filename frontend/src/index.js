import './styles.css';

import React from 'react';
import ReactDOM from 'react-dom';

import {App} from './components';

ReactDOM.render(<App />, document.getElementById('root'));

// vim: et ts=2 sw=2 :
