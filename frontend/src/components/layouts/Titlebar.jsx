import React, {memo} from 'react';
import {Typography, Toolbar, AppBar} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';

const styles = theme => ({
  link: {
    color: 'white',
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
});

export default memo(
  withStyles(styles)(props => (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="headline" color="inherit">
          <a href="/" className={props.classes.link}>
            <i>OctoStreamServer</i>
          </a>
        </Typography>
      </Toolbar>
    </AppBar>
  )),
);

// vim: et ts=2 sw=2 :
