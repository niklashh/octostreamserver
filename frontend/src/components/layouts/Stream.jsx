import React, {memo} from 'react';
import {Paper} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';

const stream_url = 'http://83.245.129.48:8080/?action=stream';
// 'http://octopi:8080/?action=stream';
const styles = theme => ({
  stream: {
    width: '100%',
    verticalAlign: 'bottom',
  },
  paper: {},
  noselect: {
    pointerEvents: 'none',
    userSelect: 'none',
  },
});

export default memo(
  withStyles(styles)(props => (
    <Paper
      square
      className={`${props.classes.paper} ${props.classes.noselect}`}>
      <img
        alt="Unable to load stream!"
        src={stream_url}
        className={`${props.classes.stream} ${props.classes.noselect}`}
      />
    </Paper>
  )),
);

// vim: et ts=2 sw=2 :
