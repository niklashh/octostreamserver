import React, {memo} from 'react';
import {Paper} from '@material-ui/core';

export default memo(props => (
  <Paper square elevation={0} style={{overflow: 'hidden'}}>
    {props.children}
  </Paper>
));

// vim: et ts=2 sw=2 :
