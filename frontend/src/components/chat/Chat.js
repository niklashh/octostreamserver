import React, {memo, useState, useEffect} from 'react';
import {
  Paper,
  TextField,
  Grid,
  List,
  ListItem,
  Typography,
  ListItemText,
  Divider,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import io from 'socket.io-client';

const socket = io(window.location.host);

const styles = theme => ({
  messagebox: {
    background: '#eee',
    borderRadius: 2 * theme.spacing.unit,
    padding: theme.spacing.unit,
    display: 'inline-flex',
    alignContent: 'left',
    color: 'white',
  },
  messagerow: {
    width: '100%',
    // marginBottom: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  sender: {
    display: 'block',
  },
  messagecontainer: {
    padding: 16,
    height: '70vh',
    overflow: 'hidden scroll',
  },
  inputfield: {
    padding: theme.spacing.unit,
    width: `calc(100% - ${2 * theme.spacing.unit}px)`,
  },
});

const sendMsg = (text, chatId, sender) =>
  fetch(`/api/chats/send/${chatId}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      sender,
      text,
    }),
  });

function hashCode(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  console.log(hash);
  return hash;
}
function hashDingDong(str) {
  let hash = 179426549;
  for (let i = 0; i < str.length; ++i) {
    hash = 15485959 * str.charCodeAt(i) + 104743 * hash;
    hash %= 32416190071;
  }
  return hash;
}
function intToHSL(i) {
  const h = (i & 0xffff) % 360,
    s = ((i >> 16) & 0xffff) % 100;
  return `hsl(${h}, ${s}%, 70%)`;
}

const Message = memo(
  withStyles(styles, {withTheme: true})(props => {
    const {text, sender, sent, classes} = props;
    return (
      <div className={classes.messagerow}>
        <div
          className={classes.messagebox}
          style={{background: intToHSL(hashDingDong(sender))}}>
          <div>
            <Typography
              variant="body2"
              color="textPrimary"
              className={classes.sender}>
              {sender}
            </Typography>
            <Typography variant="body1" color="textSecondary">
              {text}
            </Typography>
          </div>
        </div>
      </div>
    );
  }),
);

const NickDialog = props => {
  const [nick, setNick] = useState('anonymous');

  return (
    <Dialog
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Nickname</DialogTitle>
      <DialogContent>
        <DialogContentText>Change Your Nickname</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          label="Nickname"
          type="text"
          fullWidth
          onChange={event => setNick(event.target.value)}
          value={nick}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onClose} color="primary">
          Cancel
        </Button>
        <Button onClick={() => props.onSubmit(nick)} color="primary">
          Change
        </Button>
      </DialogActions>
    </Dialog>
  );
};

function useChats() {
  const [chats, setChats] = useState([]);

  function addMessage(message) {
    if (chats.length) {
      chats[1].messages.push(message);
      setChats(chats);
    }
  }

  return [chats, addMessage, setChats];
}

function scrollChat() {
  const objDiv = document.getElementById('chatlist');
  objDiv.scrollTop = objDiv.scrollHeight;
}

export default memo(
  withStyles(styles)(props => {
    const [inputValue, setInputValue] = useState('');
    const [chats, addMessage, setChats] = useChats();
    const [dialogOpen, setDialog] = useState(false);
    const [nick, setNick] = useState('anonymous');
    const {classes} = props;

    useEffect(() => {
      fetch('/api/chats')
        .then(result => result.json())
        .then(setChats)
        .catch(console.log);
    }, []);
    useEffect(
      () => {
        socket.on('chats', message => {
          addMessage(message);
          scrollChat();
        });
      },
      [chats],
    );
    useEffect(
      () => {
        console.log('Scrolling!');
        scrollChat();
      },
      [chats],
    );

    const handleKeyPress = event => {
      /* Pressing enter should submit */
      if (event.which === 13 || event.keyCode === 13) {
        if (inputValue !== '') {
          setInputValue('');
          if (chats.length) {
            sendMsg(inputValue, chats[1]._id, nick).catch(console.error);
          }
          return true;
        }
      }
      return false;
    };

    return (
      <Paper style={{padding: 0, margin: 0}}>
        <Grid container>
          <Grid xs item>
            <List className={classes.messagecontainer} id="chatlist">
              {(chats.length &&
                chats[1].messages.map((chat, index) => (
                  <Message
                    key={chat._id}
                    text={chat.text}
                    sender={chat.sender}
                    sent={chat.sent}
                  />
                ))) || (
                <ListItem>
                  <ListItemText primary="No messages" />
                </ListItem>
              )}
            </List>
          </Grid>
          <Grid xs={12} item>
            <Divider />
            <TextField
              className={classes.inputfield}
              placeholder="Write something..."
              value={inputValue}
              onChange={event => setInputValue(event.target.value)}
              onKeyPress={handleKeyPress}
            />
            <Button fullWidth onClick={() => setDialog(true)}>
              Nickname
            </Button>
            <NickDialog
              fullWidth
              onSubmit={nick => {
                console.log(nick);
                setNick(nick);
                setDialog(false);
              }}
              onClose={() => setDialog(false)}
              open={dialogOpen}
            />
          </Grid>
        </Grid>
      </Paper>
    );
  }),
);

// vim: et ts=2 sw=2 :
