import React, {memo} from 'react';
import {Grid} from '@material-ui/core';

import {Titlebar, Page, Stream} from './layouts';
import {Chat} from './chat';

export default memo(props => (
  <Page>
    <Titlebar />
    <Grid container spacing={16} style={{padding: 16}}>
      <Grid item xs={12} sm={8}>
        <Stream />
      </Grid>
      <Grid item xs={12} sm={4}>
        <Chat />
      </Grid>
    </Grid>
  </Page>
));

// vim: et ts=2 sw=2 :
