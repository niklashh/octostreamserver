const readline = require('readline');
const http = require('http');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '> ',
});
const host = 'localhost';
const api_url = `http://${host}/api`;

function parse_response(res) {
  return new Promise((resolve, reject) => {
    const statusCode = res.statusCode;
    const contentType = res.headers['content-type'];

    let error;
    if (statusCode >= 300) {
      error = new Error(`Request Failed.\nStatus Code: ${statusCode}`);
    } else if (!/^application\/json/.test(contentType)) {
      error = new Error(
        'Invalid content-type.\n' +
          `Expected application/json but received ${contentType}`,
      );
    }
    if (error) {
      // consume response data to free up memory
      res.resume();
      return reject(error);
    }

    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', chunk => (rawData += chunk));
    res.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData);
        resolve(parsedData);
      } catch (e) {
        reject(e);
      }
    });
  });
}

const get_chats = api_url =>
  new Promise((resolve, reject) =>
    http.get(`${api_url}/chats`, res =>
      parse_response(res)
        .then(data => resolve(data))
        .catch(err => reject(err)),
    ),
  );

const send_msg = (host, chat_id) => (name, message) =>
  new Promise((resolve, reject) => {
    const data = {
      sender: name,
      text: message,
    };
    const dataString = JSON.stringify(data);
    const post_req = http.request(
      {
        host,
        path: `/api/chats/send/${chat_id}`,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(dataString),
        },
      },
      res =>
        parse_response(res)
          .then(resolve)
          .catch(reject),
    );
    console.log(data);
    post_req.write(dataString);
    post_req.end();
  });

/* Application starts here */
let chat = '5c0928ee70b28203ec3c04d1';
let alias = 'anonymous';

rl.prompt();
rl.on('line', line => {
  if (line.trim() === '/get') {
    get_chats(api_url)
      .then(chats => {
        console.log(chats);
        const curChat = chats.find(c => c._id === chat);
        if (curChat) {
          console.log(`Messages in current chat (${chat}):`);
          console.log(
            curChat.messages.map(m => `${m.sender}> ${m.text}`).join('\n'),
          );
          console.log('Messages in chat:', curChat.messages.length);
        }
      })
      .then(setTimeout(rl.prompt.bind(rl), 100));
  } else if (
    line.trim().split(' ').length === 2 &&
    line.trim().split(' ')[0] === '/join'
  ) {
    chat = line.trim().split(' ')[1];
    console.log(`Joined '${chat}'`);
    rl.prompt();
  } else if (
    line.trim().split(' ').length === 2 &&
    line.trim().split(' ')[0] === '/alias'
  ) {
    alias = line.trim().split(' ')[1];
    console.log(`Your alias is '${alias}'`);
    rl.prompt();
  } else {
    if (chat && alias) {
      send_msg(host, chat)(alias, line)
        .then(rl.prompt.bind(rl))
        .catch(error => {
          console.log(error.message);
          rl.prompt();
        });
    } else {
      console.log('Please select a chat and an alias');
      rl.prompt();
    }
  }
}).on('close', () => {
  console.log('Have a great day!');
  process.exit(0);
});

// vim: et ts=2 sw=2 :
