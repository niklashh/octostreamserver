import requests, json
URL = 'http://localhost/api'

def get_chats(api_url):
    chats = json.loads(requests.get(f'{api_url}/chats').text)
    return chats

def send_msg(api_url, chatid, data):
    return requests.post(f'{api_url}/chats/send/{chatid}', data).text

if __name__=='__main__':
    try:
        info = map(lambda d: (d['_id'], d['title']), get_chats(URL))
        print('\n'.join(f'ChatId: {_id}\n  Title: {title}' for _id, title in info))
        chat = input('Which ChatId: ')
        name = input('Alias: ')
        while 1:
            msg = input(f'{name}> ')
            send_msg(URL, chat, {'sender': name, 'text': msg})
    except KeyboardInterrupt:
        print('Quit')
        quit()
