# OctoStreamServer

Octoprint camera stream server

Contains a chat backend

## How to start

1. Generate random keys to the [secrets.env](secrets.env) file
2. Start docker-compose using those keys in the environment with `./dcup.sh`