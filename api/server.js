const http = require('http');
const app = require('./app');

const PORT = process.env.API_PORT || 5000;
const server = http.createServer(app);
const io = require('socket.io')(server);

app.io = io;

server.listen(PORT, () => {
  console.log(`Server started on port ${PORT}!`);
});

// vim: et ts=2 sw=2 :
