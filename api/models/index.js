const chat = require('./chat');

module.exports = {
  chat,
};

// vim: et ts=2 sw=2 :
