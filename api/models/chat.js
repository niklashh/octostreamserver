const mongoose = require('mongoose');

const messageSchema = mongoose.Schema({
  text: {type: String, required: true},
  sender: {type: String, required: true},
  sent: {type: Date, default: Date.now, required: true},
});

const chatSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
    validate: {
      isAsync: true,
      validator: function(v, cb) {
        return cb(
          /^[a-z]{2,20}$/.test(v),
          'Title must be 2-20 lowercase letters',
        );
      },
    },
  },
  messages: {
    type: [messageSchema],
    default: [],
  },
});

chatModel = mongoose.model('Chat', chatSchema);

module.exports = {
  messageSchema,
  chatSchema,
  chatModel,
};

// vim: et ts=2 sw=2 :
