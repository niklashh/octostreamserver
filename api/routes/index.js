const chatsRouter = require('./chats');
const octoprintRouter = require('./octoprint');

module.exports = {
  chatsRouter,
  octoprintRouter,
};

// vim: et ts=2 sw=2 :
