const express = require('express');
const mongoose = require('mongoose');
const serializeError = require('serialize-error');

const Chat = require('../models').chat.chatModel;

const router = express.Router();

/**
 * @swagger
 * tags:
 *   - name: Chats
 *     description: Operations with the chat application
 */

/**
 * @swagger
 * definitions:
 *   Chat:
 *     properties:
 *       _id:
 *         type: string
 *         format: ObjectId
 *         example: 5c0901acda2406039203b428
 *       title:
 *         type: string
 *         pattern: '^[a-z]{2,20}$'
 *       messages:
 *         type: array
 *         items:
 *           type: object
 *           properties:
 *             _id:
 *               type: string
 *               format: ObjectId
 *               example: 5c0a1453228171002cb3dbdc
 *             text:
 *               type: string
 *             sender:
 *               type: string
 *             sent:
 *               type: string
 *               format: date-time
 *           required:
 *             - text
 *             - sender
 *             - sent
 *     required:
 *       - title
 *       - messages
 */

/**
 * @swagger
 * /chats:
 *   get:
 *     tags:
 *       - Chats
 *     description: Returns all chats
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A list of chats
 *         schema:
 *           $ref: '#/definitions/Chat'
 */
router.get('/', (req, res, next) => {
  Chat.find()
    .then(chats => {
      res.status(200).json(chats);
    })
    .catch(error =>
      // Pass the error to error handler
      next(error),
    );
});

/**
 * @swagger
 * /chats:
 *   post:
 *     tags:
 *       - Chats
 *     description: Create a chat
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: body
 *         schema:
 *           properties:
 *             title:
 *               type: string
 *               pattern: '^[a-z]{2,20}$'
 *     responses:
 *       201:
 *         description:
 *           A new chat was created\
 *
 *           By default returns an empty list of messages
 *         schema:
 *           $ref: '#/definitions/Chat'
 *       400:
 *         description: Bad request. The given title is invalid
 */
router.post('/', (req, res, next) => {
  new Chat({
    title: req.body.title,
  })
    .save()
    .then(result => {
      res.status(201).json(result);
    })
    .catch(error => {
      if (error.name === 'ValidationError') {
        res.status(400).json({error: serializeError(error)});
      } else {
        next(error);
      }
    });
});

/**
 * @swagger
 * /chats/{id}:
 *   delete:
 *     tags:
 *       - Chats
 *     description: Delete a chat
 *     parameters:
 *       - in: path
 *         description: ObjectId (_id) of a chat
 *         name: id
 *         required: true
 *         schema:
 *           type: ObjectId
 *           example: 5c0940a3491f2401802aacae
 *     responses:
 *       200:
 *         description: The chat was deleted
 *       400:
 *         description: Bad request. The given ObjectId is incorrect
 */
router.delete('/:ChatId', (req, res, next) => {
  Chat.findByIdAndDelete(req.params.ChatId)
    .then(result => {
      if (result) {
        res.status(200).json({...result, message: 'Document deleted'});
      } else {
        res.status(400).json({message: 'Invalid chat ObjectId'});
      }
    })
    .catch(error => {
      next(error);
    });
});

/**
 * @swagger
 * /chats/{id}:
 *   patch:
 *     tags:
 *       - Chats
 *     description: Modify a chat
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: body
 *         schema:
 *           $ref: '#/definitions/Chat'
 *       - in: path
 *         description: ObjectId (_id) of a chat
 *         name: id
 *         required: true
 *         schema:
 *           type: ObjectId
 *           example: 5c0940a3491f2401802aacae
 *     responses:
 *       200:
 *         description:
 *           The document was modified\
 *
 *           Returns the previous document
 *         schema:
 *           $ref: '#/definitions/Chat'
 *       400:
 *         description:
 *           Bad request.\
 *
 *           The given ObjectId was invalid or
 *           the given fields were invalid
 */
router.patch('/:ChatId', (req, res, next) => {
  Chat.findByIdAndUpdate(req.params.ChatId, req.body, {runValidators: true})
    .then(result => {
      if (result) {
        // Returns the previous document
        res.status(200).json(result);
      } else {
        res.status(400).json({message: 'Invalid chat ObjectId'});
      }
    })
    .catch(error => {
      if (error.name === 'ValidationError' || error.name === 'CastError') {
        res.status(400).json({error: serializeError(error)});
      } else {
        next(error);
      }
    });
});

/**
 * @swagger
 * /chats/send/{id}:
 *   post:
 *     tags:
 *       - Chats
 *     description: Send a message to a chat
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: body
 *         schema:
 *           properties:
 *             text:
 *               type: string
 *             sender:
 *               type: string
 *       - in: path
 *         description: ObjectId (_id) of a chat
 *         name: id
 *         required: true
 *         schema:
 *           type: ObjectId
 *           example: 5c0940a3491f2401802aacae
 *     responses:
 *       201:
 *         description: A new message was sent and created
 *         schema:
 *           $ref: '#/definitions/Chat'
 *       400:
 *         description:
 *           Bad request. The given message is invalid\
 *
 *           or the given ChatId is invalid
 */
router.post('/send/:ChatId', (req, res, next) => {
  Chat.findById(req.params.ChatId)
    .then(chat => {
      if (!chat) {
        res.status(400).json({message: 'Invalid chat ObjectId'});
        return;
      }
      chat.messages.push(req.body);
      // This is not atomic
      const index = chat.messages.length - 1;
      return chat.save().then(result => {
        res.status(201).json(result);
        req.app.io.emit('chats', result.messages[index]);
      });
    })
    .catch(error => {
      if (error.name === 'ValidationError' || error.name === 'CastError') {
        res.status(400).json({error: serializeError(error)});
      } else {
        next(error);
      }
    });
});

module.exports = router;

// vim: et ts=2 sw=2 :
