const express = require('express');
const serializeError = require('serialize-error');
const axios = require('axios');

const router = express.Router();

const OCTOPI_URL = 'http://octopi';
const OCTOPI_API_KEY = process.env.OCTOPI_API_KEY;

function redirect(response) {
  this.status(response.status).json(response.data);
}

/**
 * @swagger
 * tags:
 *   - name: Octoprint
 *     description: Operations to interface with the octoprint server API
 *     externalDocs:
 *       url: http://docs.octoprint.org/en/master/api/index.html
 */

/**
 * @swagger
 * /octoprint/version:
 *   get:
 *     tags:
 *       - Octoprint
 *     description: Returns information regarding server and API version
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: No error
 *         schema:
 *           properties:
 *             api:
 *               type: string
 *               example: "0.1"
 *             server:
 *               type: string
 *               examgle: "1.3.10"
 *       403:
 *         description: Invalid API key
 */
router.get('/version', (req, res, next) => {
  axios
    .get(`${OCTOPI_URL}/api/version`, {
      headers: {'X-Api-Key': OCTOPI_API_KEY},
    })
    .then(response => redirect.bind(res)(response))
    .catch(error => {
      if (error.response) {
        redirect.bind(res)(error.response);
      } else if (error.request) {
        // If for example the host address couldn't be resolved
        next(error);
      } else {
        // Undefined error branch
        console.log(error);
        res
          .status(500)
          .json(
            'Something unexpected happened! Check the log for more information',
          );
      }
    });
});

module.exports = router;

// vim: et ts=2 sw=2 :
