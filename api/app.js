const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const serializeError = require('serialize-error');
const swaggerJSDoc = require('swagger-jsdoc');

const {chatsRouter, octoprintRouter} = require('./routes');

// Do not end PREFIX with a /
const PREFIX = process.env.API_ROUTE_PREFIX;
const app = express();
const swaggerDefinition = {
  info: {
    title: 'OctoStreamServer API',
    version: '0.1.2',
    description: 'API documentation for the express backend',
  },
  host: 'localhost',
  basePath: PREFIX,
};
const options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./routes/*.js'],
};
const swaggerSpec = swaggerJSDoc(options);

mongoose.connect(
  `mongodb://admin:${process.env.MONGO_ADMIN_PASSWORD}@database/octostream`,
);
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Serve swagger.json
app.get(`${PREFIX}/swagger.json`, function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

// Route specific middleware
app.use(`${PREFIX}/chats`, chatsRouter);
app.use(`${PREFIX}/octoprint`, octoprintRouter);

// Not found handler
app.use((req, res, next) => {
  const error = new Error(`Route not found ${req.method} ${req.url}!`);
  error.status = 404;
  next(error);
});

// Error handler
// If you see an error in the console,
// but not in the response, that means
// there's an error happening after
// the response is sent
app.use((err, req, res, _) => {
  // If a user is trying to modify
  // the _id field in a document
  if (err.code === 66) {
    res.status(400).json(serializeError(err));
    return;
  }
  // General error handling
  const status = err.status || 500;
  res.status(status);
  if (status === 500) {
    console.log(err);
  }
  res.json({
    error: serializeError(err),
  });
});

module.exports = app;

// vim: et ts=2 sw=2 :
